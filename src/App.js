import {useState,useEffect} from 'react'
// import {Fragment} from 'react'
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'

import Home from './pages/Home'
import Courses from './pages/Courses'
import CourseView from './pages/CourseView'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error';
import {UserProvider} from './UserContext'
import './App.css';

function App() {


	const [user,setUser]=useState({
		id:null,
		isAdmin:null
		// email : localStorage.getItem('email')
	})
	const unsetUser = () =>{
		localStorage.clear()
	}

	useEffect(() =>{
		let token = localStorage.getItem('token')
		fetch('http://localhost:4000/users/details',{
			method : "GET",
			headers:{
				"Authorization": `Bearer ${token}`
			}
		})
		.then(res=> res.json())
		.then(data =>{
			if(typeof data._id !== "undefined"){
				setUser({
					id:data._id,
					isAdmin : data.isAdmin
				})
			}
			else {
				setUser({
					id:null,
					isAdmin:null
				})
			}
		})
		// console.log(user)
		// console.log(localStorage)
	},[user])
// The UserProvider component is what allows other components to consume or use our context. Any component which is not wrapped by UserProvider will not have access to the values provided for our context
// You can pass data or information to our context by providing a "value" attribute in our UserProvider. Data passed here can be accessed by other components by unwrapping our context using the useContext hook
		return (
/*			<Fragment>
				<AppNavbar/>
				<Login/>
				<Register/>
				<Home/>
				<Courses/>
			</Fragment> */
			<UserProvider value={{user,setUser,unsetUser}}>
			<Router>
				<AppNavbar/>
				<Container>
					<Switch>
						<Route exact path="/" component = {Home} />
						<Route exact path="/courses" component = {Courses}/>
						<Route exact path="/courses/:courseId" component = {CourseView}/>
						<Route exact path="/login" component = {Login}/>
						<Route exact path="/register" component = {Register}/>
						<Route exact path="/logout" component = {Logout}/>
						<Route component={Error} />
						
					</Switch>
				</Container>
			</Router>
			</UserProvider>

		)
}

export default App;
