import {Form,Button} from 'react-bootstrap'
import {useState, useEffect,useContext} from 'react'
import UserContext from '../UserContext'
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Register(){
	const {user, setUser} = useContext(UserContext)
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [email, setEmail] = useState('')
	const [firstName, setFname] = useState('')
	const [lastName, setLname] = useState('')
	const [mobileNo, setMobile] = useState('')

	const [isActive, setIsActive] = useState(false)
	useEffect(() =>{
		if((email !== '' && password1!=='' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNo !== '' ) && (password1===password2)){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	},[email,password1,password2,firstName,lastName,mobileNo])

	function registerUser(e){
		e.preventDefault();
		fetch('http://localhost:4000/users/register',{
			method: 'POST',
			headers:{
				'Content-Type' :'application/json'
			},
			body: JSON.stringify({
				firstName : firstName,
				lastName : lastName,
				email : email,
				mobileNo : mobileNo,
				password : password1
			})
		})
		.then(res =>res.json())
		.then(data =>{
			console.log(data)
			if(data === true){
				setEmail('')
				setPassword1('')
				setPassword2('')
				setFname('')
				setLname('')
				setMobile('')
			Swal.fire({
					title:'Account Successfully registed',
					icon:'succes',
					text: 'Welcome to Zuitt'
				})
		} 
		else {
				Swal.fire({
					title:'Some inputs are invalid.',
					icon:'error',
					text: 'Check your details'
				})
			}	
		})

		// localStorage.setItem('email',email)
		
	}
	return(
			(user.id !== null)?
			<Redirect to = "/courses"/>:
			<Form onSubmit= {(e)=>registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control
					type= 'string'
					placeholder = 'Please enter your First name here'
					required
					value = {firstName}
					onChange = {e => setFname(e.target.value)}
					/>
				<Form.Text className = "text-muted">
				We'll never share your details with anyone else.
				</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control
					type= 'string'
					placeholder = 'Please enter your Last name here'
					required
					value = {lastName}
					onChange = {e => setLname(e.target.value)}
					/>
				<Form.Text className = "text-muted">
				We'll never share your details with anyone else.
				</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile number:</Form.Label>
					<Form.Control
					type= 'number'
					placeholder = 'Please enter your Mobile number here'
					required
					value = {mobileNo}
					onChange = {e => setMobile(e.target.value)}
					/>
				<Form.Text className = "text-muted">
				We'll never share your details with anyone else.
				</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control
					type= 'email'
					placeholder = 'Please enter your email here'
					required
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					/>
				<Form.Text className = "text-muted">
				We'll never share your email with anyone else.
				</Form.Text>
				</Form.Group>
				<Form.Group controlId = "password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control
				type = 'password'
				placeholder = 'Please input your password'
				value = {password1}
				onChange = {e => setPassword1(e.target.value) }
				required

				/>
				</Form.Group>
				<Form.Group controlId= 'password2'>
				<Form.Label>Verify password :</Form.Label>
				<Form.Control
				type = 'password'
				placeholder = 'Please verify your password'
				value = {password2}
				onChange = {e =>setPassword2(e.target.value)}
				required

				/>
				</Form.Group>
				{ isActive ?
					<Button variant = 'primary' type = 'submit' id= 'submitBtn'>
					Register
					</Button>
					:
					<Button variant = 'danger' type = 'submit' id= 'submitBtn' disabled>
					Register
					</Button>
				}
				
			</Form>

		)
}