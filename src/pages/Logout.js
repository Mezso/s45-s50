import {useEffect,useContext} from 'react'
import {Redirect} from 'react-router-dom'
import UserContext from '../UserContext'
export default function Logout(){

	// Consume the UserContext object and destructure it to access the user state and unsetUser function from the context provider
	const {unsetUser,setUser}= useContext(UserContext)
	// localStorage.clear()
	unsetUser()


	// placing the "setUser" setter function inside the useEffect is necessary because of updates within ReactJS that a state of another component cannot be updated while trying to render a different component
	// By adding the useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of our user.
	useEffect(() =>{
		setUser({id:null})
	})
	return(

		<Redirect to='/login'/>

		)
}