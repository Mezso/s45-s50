import {Fragment} from 'react'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'


export default function home (){

	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunity for everyone, everywhere",
		destination: "/",
		label: "Enroll now!"
	}
	return(
		<Fragment>
				<Banner data = {data}/>
				<Highlights/>
				
		</Fragment>
		)
}