import {Form,Button} from 'react-bootstrap'
import {useState, useEffect,useContext} from 'react'
import {Redirect} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function Login(){

	const {user, setUser} = useContext(UserContext)
	const [isActive, setIsActive] = useState(false)
	const [password, setPassword] = useState('')
	const [email, setEmail] = useState('')
	useEffect(() =>{
		if((email !== '' && password!=='')){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	},[email,password])


	function Login(e){
		e.preventDefault();
		fetch('http://localhost:4000/users/login',{
			method: 'POST',
			headers:{
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password:password
			})

		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			if(typeof data.access !== "undefined"){
				localStorage.setItem('token',data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title:'Login Successful',
					icon:'succes',
					text: 'Welcome to Zuitt'
				})
			} else{
				Swal.fire({
					title:'Authentication failed.',
					icon:'error',
					text: 'Check your login details'
				})
			}
		})
		// localStorage.setItem('email', email)
		// setUser({
		// 	email:localStorage.getItem('email')
		// })
		setEmail('')
		setPassword('')
	}
	const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details', {
            headers:{
                  Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

	return(
		(user.id !== null)?
		<Redirect to = "/courses"/>:

		<Form onSubmit= {(e)=>Login(e)}>
			<Form.Group controlId = "email">
				<Form.Label>
				Email Address:
				</Form.Label>
				<Form.Control
				type = 'email'
				placeholder = 'Enter your Email'
				value = {email}
				onChange = {e => setEmail(e.target.value)}
				required
			
				/>
			</Form.Group>
			<Form.Group controlId = "password">
				<Form.Label>
				Password:
				</Form.Label>
				<Form.Control
				type = 'password'
				placeholder = 'Enter your password'
				value = {password}
				onChange = {e => setPassword(e.target.value) }
				required
				
				/>
			</Form.Group>
			<Form.Group>
			{ isActive ?
					<Button variant = 'primary' type = 'submit' id= 'submitBtn'>
					Login
					</Button>
					:
					<Button variant = 'danger' type = 'submit' id= 'submitBtn' disabled>
					Login
					</Button>
				}
			</Form.Group>
			
		</Form>

		)
}