import {useState} from 'react'
import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}){
	// console.log(courseProp)
	const {name, description, price, _id} = courseProp
	const [count,setCount] = useState(0)
	// const [seat,seatCount] = useState(30)
	//  const[getter,setter] = useState(initialValue)
	// console.log(useState(0))
	// hook used is useState to store the stare
	// function enroll(){
	// 	if(count <30 && seat > 0){
	// 		setCount(count +1)
	// 		seatCount(seat -1)
	// 		console.log('Enrollees'+count)
	// 	}
	// 	else {
	// 		alert('The class is full')
	// 	}
		
	// }
		return(

			<Card >
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{price}</Card.Text>
{/*					<Card.Text>Enrollees:{count} Seat: {seat}</Card.Text>*/}
					<Link className ="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
				</Card.Body>
			</Card>
	
	
	)
}